import timeit
from pathlib import Path

if (Path(__file__).parent / "test_PHYSLITE.py").exists():
    print("PHYSLITE time tests\n")
    # Importing callables runs all code not in a function or
    # guarded by "__main__" which provides the setup necessary
    # to run the test functions.
    from test_PHYSLITE import test_atlas_cpp, test_numpy_op

    n_trials = 10000
    time_atlas_cpp = timeit.timeit(test_atlas_cpp, number=n_trials)
    time_numpy_op = timeit.timeit(test_numpy_op, number=n_trials)

    print(f"# Arithmetic mean time for {n_trials} trials:")
    print(f"# ATLAS C++: {time_atlas_cpp/n_trials:.5f} sec")
    print(f"# NumPy operation: {time_numpy_op/n_trials:.5f} sec")
    print(f"# ATLAS C++/NumPy: {time_atlas_cpp/time_numpy_op:.5f}")
