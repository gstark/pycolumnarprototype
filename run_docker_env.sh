#!/bin/bash

RUNNER_IMAGE="gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.3"

docker pull "${RUNNER_IMAGE}"

# As default user 'atlas' is uid 1000
# ```
# $ docker run --rm gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.3 /bin/bash -c 'id'
# uid=1000(atlas) gid=1000(atlas) groups=1000(atlas),0(root),10(wheel)
# ```
# match it with `--user 1000:1000`` to ensure that output files from bindmount are under user control

# Get local development environment
docker run \
    --rm \
    -ti \
    --user 1000:1000 \
    --volume $PWD:/workdir \
    "${RUNNER_IMAGE}"
