#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>
#include <nanobind/stl/string.h>

#include "ColumnarPrototype/ColumnarExampleTool.h"

namespace nb = nanobind;

class ToolHolder{
    public:
        ToolHolder() : m_tool(std::make_unique<col::ColumnarExampleTool>("MyTool")){
        }

        template<typename T>
        void setColumn(const std::string& key, nb::ndarray<T> column){
            this->m_tool->setColumn (key, column.shape(0), column.data());
            auto size_and_buf = this->m_tool->getColumn<const T>(key);
        }

        void compute(col::ColumnarOffsetType begin, col::ColumnarOffsetType end){
            this->m_tool->checkColumnsValid ();
            this->m_tool->calculateVector (col::ObjectRange<col::ObjectType::muon>(begin, end));
        }

    private:
        std::unique_ptr<col::ColumnarExampleTool> m_tool;
};

nb::ndarray<nb::numpy, float> column_maker(){

    // copied from columnarprototype/util/simple_columnar_example.cxx
    const std::string toolName = "MyTool";
    auto tool = std::make_unique<col::ColumnarExampleTool>(toolName);

    std::vector<float> pt, eta;
    std::vector<float>* output = new std::vector<float>{ };
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output->resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output->size(), output->data());
    std::vector<col::ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    tool->checkColumnsValid ();
    tool->calculateVector (col::ObjectRange<col::ObjectType::muon>(0, offsets.back()));

    // Delete 'output' when the 'owner' capsule expires
    nb::capsule owner(output, [](void *p) noexcept {
        delete[] (float *) p;
    });

    int ndim = 1;
    size_t shape[ndim] = {output->size()};  // NumPy-like .shape: (output->size(), )

    return nb::ndarray<nb::numpy, float>(output->data(), ndim, shape, owner);
}

NB_MODULE(PyColumnarPrototype, m) {
    m.doc() = "nanobind example plugin"; // optional module docstring
    nb::class_<ToolHolder>(m, "ToolHolder")
        .def(nb::init<>())
        .def("setColumns", &ToolHolder::setColumn<float>)
        .def("setColumns", &ToolHolder::setColumn<col::ColumnarOffsetType>)
        .def("compute", &ToolHolder::compute);
    m.def("column_maker", &column_maker, "A function that produces output from ColumnarExampleTool");
}
