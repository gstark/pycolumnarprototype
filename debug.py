import awkward as ak
import numpy as np
import PyColumnarPrototype


class MuonTool:
    def __init__(self):
        self._tool = PyColumnarPrototype.ToolHolder()

    def compute(self, muons):
        pt = np.asarray(muons.pt.layout.content, np.float32)
        eta = np.asarray(muons.eta.layout.content, np.float32)
        offsets = np.asarray(muons.pt.layout.offsets, np.uint)

        outputs = np.empty(pt.shape, np.float32)

        self._tool.setColumns("Muons_eta", eta)
        self._tool.setColumns("Muons_pt", pt)
        self._tool.setColumns("Muons", offsets)

        self._tool.setColumns("Muons_output", outputs)
        self._tool.compute(offsets[0], offsets[-1])

        return ak.Array(
            ak.contents.ListOffsetArray(
                offsets=ak.index.Index64(offsets),
                content=ak.contents.NumpyArray(outputs),
            )
        )

muoncptool = MuonTool()

muons = ak.Array(
    [
        [
            {"pt": 20000.0, "eta": -2.1},  # 2 muons in event 0
            {"pt": 40000.0, "eta": -1.1},
        ],
        [],  # 0 muons in event 1
        [{"pt": 40000.0, "eta": -1.1}],  # 1 muon in event 2
    ]
)
result = muoncptool.compute(muons)

result_title = "Result"
print(f"\n# {result_title: <11}: {result}")
print(f"# Expectation: {muons.pt * np.cosh(muons.eta)}")

# print(PyColumnarPrototype.column_maker())
