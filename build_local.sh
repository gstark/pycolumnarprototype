#!/bin/bash

_ANALYSISBASE_VERSION=24.2.3
if [ -d "/cvmfs/atlas.cern.ch" ]; then
    if [ "$(basename -- $0)" = "$(basename ${BASH_SOURCE})" ]; then
        # running in subshell
        # Define setupATLAS and asetup
        ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
        function setupATLAS {
            source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
        }
    fi

    setupATLAS

    asetup AnalysisBase,"${_ANALYSISBASE_VERSION}"

elif [ -f "/release_setup.sh" ]; then
    # If in Linux container
    . /release_setup.sh
fi

# Ensure Python dependencies satisfied
python3 -c 'import awkward' &> /dev/null
if [ $? -eq 1 ]; then
    if [ ! -d .venv ]; then
        bash <(curl -sL https://raw.githubusercontent.com/matthewfeickert/cvmfs-venv/v0.0.4/cvmfs-venv.sh) .venv
    fi
    . .venv/bin/activate
    python3 -m pip install --upgrade --requirement requirements.txt
fi

cmake \
    -S src \
    -B build
cmake build -LH
cmake \
    --build build \
    --clean-first \
    --parallel "$(nproc --ignore=4)"

# Location is non-standard across OS
export PYTHONPATH="$(dirname $(find . -type f -iname "PyColumnarPrototype*.so")):${PYTHONPATH}"
printf "\n# PYTHONPATH: ${PYTHONPATH}\n\n"

python3 -c 'import PyColumnarPrototype; print(f"{PyColumnarPrototype.column_maker()=}")'
